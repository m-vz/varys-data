This repository contains all audio recordings captured by `varys` in `.opus` format and the database in `database.sql`.

The `ml` directory contains some of the trained models.

The `plots` directory contains the traffic plots for the `small` and `binary` datasets.

Due to limited storage quota, the corresponding traffic traces had to be uploaded in a separate repository: https://gitlab.com/m-vz/varys-captures.
